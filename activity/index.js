// alert("hi")

// in this line of code you are getting the document in the HTML element with the id txt-first-name

const txtFirstName = document.querySelector("#txt-first-name")

const txtLastName = document.querySelector("#txt-last-name")
// alternative way of targeting an element
// document.getElementById("#txt-first-name")
// document.getElementByClassName()
// document.getElementByTagName()

// Mini Activity

const spanFullName = document.querySelector("#span-full-name")


txtFirstName.addEventListener("keyup", printFullName)


txtLastName.addEventListener("keyup", printFullName)


function printFullName(event) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
};